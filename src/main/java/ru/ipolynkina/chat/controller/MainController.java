package main.java.ru.ipolynkina.chat.controller;

import main.java.ru.ipolynkina.chat.domen.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/controller")
public class MainController {

    @GetMapping
    public Flux<Message> messages(
            @RequestParam(defaultValue = "0") Long start,
            @RequestParam(defaultValue = "3") Long count
    ) {
        return Flux
                .just(
                        "Hello",
                        "111",
                        "222",
                        "333",
                        "444",
                        "555"
                )
                .skip(start)
                .take(count)
                .map(Message::new);
    }
}
